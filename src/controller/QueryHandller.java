package controller;

import model.MonitoredData;
import parser.FileParser;
import utils.Utils;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;

//Stream and Collectors
import java.util.stream.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class QueryHandller {
    private FileParser parser = new FileParser();
    private List<MonitoredData> activities;

    public QueryHandller() {
        this.activities = parser.readFile();
        System.out.println(activities);
    }

    public Long countDays(){
        Long days=this.activities.stream()
                .map((MonitoredData a) -> Utils.convert_local_date_time_into_days(a.getStartTime()))
                .distinct()
                .count();
        System.out.println("Number of days: "+days);
        return days;
    }

    public Map<String,Integer> countActivityFrequency(){
        List<String> labels = activities.stream()
                .map(MonitoredData::getLabel)
                .collect(Collectors.toList());

        Map<String, Integer> activities_frequencies =
                labels.stream().collect(groupingBy(Function.identity(), summingInt(e -> 1)));
        System.out.println("Activities Frequencies :"+activities_frequencies);
        return activities_frequencies;
    }

    public Map<String, LocalDateTime> mapActivityDuration(){
        Map<String, LocalDateTime> activities_duration = activities.stream()
                .collect(Collectors.toMap(activity->activity.getLabel()+activity.getIndex(),
                        activity-> activity.get_difference_between_local_times()));

        System.out.println(activities_duration);
        return activities_duration;
    }



    public static void main(String[] args) {
        QueryHandller q = new QueryHandller();
        q.countDays();
        q.countActivityFrequency();
       // q.getReindexedList();
        q.mapActivityDuration();

    }
}
