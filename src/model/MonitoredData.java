package model;

//Date classes
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import utils.Utils;


public class MonitoredData {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activity_label;
    private int index_activity=0;
    private static int index=0;

    public MonitoredData(String startTime, String endTime, String label) {
        this.startTime = Utils.convert_string_into_local_time(startTime);
        this.endTime = Utils.convert_string_into_local_time(endTime);
        this.activity_label = label;
        this.index_activity =index;
        MonitoredData.index+=1;
    }


    public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String label,int index) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity_label = label;
        MonitoredData.index =index;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public void setLabel(String label) {
        this.activity_label = label;
    }

    public String getLabel() {
        return activity_label;
    }

    public LocalDateTime get_difference_between_local_times(){
        long difference_in_seconds=this.startTime.until(this.endTime, ChronoUnit.SECONDS);
        // 1970-01-01 este 0 in termeni de date
        LocalDateTime time_difference = LocalDateTime.ofInstant(Instant.ofEpochSecond(difference_in_seconds), ZoneId.systemDefault());

        //System.out.println(time_difference);
        return time_difference;
    }

    public int getIndex() {
        return index_activity;
    }

    public void setIndex(int index) {
        this.index_activity = index;
    }

    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String start = startTime.format(formatter).replace("T"," ");
        String end = endTime.format(formatter).replace("T"," ");

        return "Activity{" +
                "index='" + index_activity +
                ", label='" + activity_label +
                ", startTime=" + start+
                ", endTime=" + end+ '\''+
                '}'+'\n';
    }

    /*
    
    public static void main(String[] args) {
        MonitoredData a = new MonitoredData("2011-11-28 10:21:24","2011-11-28 10:23:36", "Showering");
        MonitoredData b = new MonitoredData("2011-11-28 10:21:24","2011-11-28 10:23:36", "Sleeping");
        System.out.println(a);
        System.out.println(b);
        System.out.println(Utils.difference_between_local_times(a.getStartTime(),a.getEndTime()));
    }
    */

}
