package parser;

import model.MonitoredData;

import java.util.List;

//IO classes
import java.nio.file.Paths;
import java.nio.file.Files;

//Stream and Collectors
import java.util.stream.Collectors;


//Exception Classes
import java.io.IOException;

public class FileParser {

    private  String PATH="src/activities.txt";

    public List<MonitoredData> readFile(){
        List<MonitoredData> activities = null;
        try {
            activities = Files.lines(Paths.get(PATH))
                    .map(s -> s.split("\t\t"))
                    .map(arr -> new MonitoredData(arr[0],arr[1], arr[2].replaceAll("\\s+","")))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return activities;

    }


    /*
    //Testing Purposes
    public static void main(String[] args) {
        System.out.println(new FileParser().readFile());
       }
     */

}
