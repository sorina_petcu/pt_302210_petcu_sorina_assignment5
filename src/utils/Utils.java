package utils;

//Date classes
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;

@SuppressWarnings("unused")
public class Utils {
    public static LocalDateTime convert_string_into_local_time(String time_string){
        //System.out.println(time_string);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime time = LocalDateTime.parse(time_string,formatter);
        return time;
    }
    public static String convert_local_date_time_into_days(LocalDateTime time){
        return time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).substring(0,10);
    }

    public static LocalDateTime difference_between_local_times(LocalDateTime startTime, LocalDateTime endTime){
        long difference_in_seconds=startTime.until(endTime, ChronoUnit.SECONDS);

        /*
        int hours = (int) difference_in_seconds / 3600;
        int remainder = (int) difference_in_seconds- (hours * 3600);
        int minutes = (int) remainder / 60;
        remainder -= (int) (minutes * 60);
        int seconds = remainder;

        String format="yyyy-MM-dd HH:mm:ss";
        String time_string;
        if(hours>0){
            format = "yyyy-MM-dd HH:mm:ss";
            time_string = hours+":"+minutes+":"+seconds;
        }else if(minutes>0){
            format = "mm:ss";
            time_string = minutes+":"+seconds;
        }else{
            format = "ss";
            time_string = ""+seconds;
        }*/

        // 1970-01-01 is 0 in terms of date
        LocalDateTime time_difference = LocalDateTime.ofInstant(Instant.ofEpochSecond(difference_in_seconds), ZoneId.systemDefault());

        System.out.println(time_difference);
        return time_difference;
    }

    /*
    //Testing Purposes
    public static void main(String[] args) {
        System.out.println(hours+" "+mins+" "+sec);
    }
     */



}
